@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard APP</div>

                <div class="panel-body">
                    @section('content')
                        <div class="container">
                            <div class="col-sm-offset-2 col-sm-8">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Dashboard
                                    </div>

                                    <div class="panel-body">
                                        <!-- Display Validation Errors -->
                                    @include('common.errors')

                                    <!-- New Task Form -->
                                        <form class="form-horizontal">
                                        {{ csrf_field() }}

                                        <!-- Task Name -->
                                            <div class="form-group">
                                                <label for="task-name" class="col-sm-3 control-label">Buttons</label>
                                            </div>

                                            <!-- Add Task Button -->
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-6">
                                                    <button href="{{ url('') }}" class="btn btn-default">
                                                        <i class="fa fa-btn fa-plus"></i>Button 1
                                                    </button>
                                                    <br>
                                                    <br>
                                                    <button type="submit" class="btn btn-default">
                                                        <i class="fa fa-btn fa-plus"></i>Button 2
                                                    </button>
                                                    <br>
                                                    <br>
                                                    <button type="submit" class="btn btn-default">
                                                        <i class="fa fa-btn fa-plus"></i>Button 3
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    @endsection                </div>
            </div>
        </div>
    </div>
</div>
@endsection
